# FileToArrayConverter

Created by Jelmer Pijnappel - ChocolatePinecone (29-08-2019)

## Description
This tool simply reads a file and returns the contents in c++ char array notation (e.g. 'h', 'e', 'l', 'l', 'o')
This can be used for hardcoding resources such as music or textures in C++ projects.