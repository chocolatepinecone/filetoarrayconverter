#include "pch.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <memory>

using namespace std;

// Get character array from file
std::shared_ptr<std::vector<char>> getCharsFromFile(std::string filePath) {
	// Open the file
	std::ifstream in;
	in.open(filePath, std::ios::binary);

	if (in.is_open()) {
		// Allocate a char array for storing the file data
		in.seekg(0, std::ios::end); // Go to the end of the file
		unsigned int length = (unsigned int)in.tellg(); // Report location of the end (this is the length fo the file)
		in.seekg(0, std::ios::beg); // Go back to the beginning for reading
		std::shared_ptr<std::vector<char>> buffer = std::make_shared< std::vector<char>>(length); // Allocate memory for a buffer of appropriate size
		in.read(&buffer->at(0), length); // Read the whole file into the buffer

		in.close();

		return buffer;
	}

	cout << "File " + filePath + " could not be opened. Did you fill in the file name correctly, including extension type?" << endl;
	return nullptr;
}

// Main function
int main() {
	string path, newFileName;
	std::shared_ptr<std::vector<char>> readBytes;

	// Ask for path to file
	cout << "Which file do you want to convert to a C++ array?" << endl;
	cin >> path;
	newFileName = path + ".convertedarray";

	// Read file
	readBytes = getCharsFromFile(path);

	if (readBytes != nullptr) {
		// Open new file for writing
		ofstream myNewfile;
		myNewfile.open(newFileName, ios::binary);

		if (myNewfile.is_open())
		{
			// Write out every char in array constructor notation
			for (size_t i = 0; i < readBytes->size(); i++) {
				// Get character
				char& c = readBytes->at(i);

				// Write character ascii int value to file in array notation
				std::string notation = std::to_string(c);
				notation += (i < readBytes->size() - 1) ? "," : "";
				myNewfile.write(&notation.front(), notation.size());

				// Update console with current status
				if (i % 200 == 0) {
					std::cout << "Converted: " + std::to_string(i) + "/" + std::to_string(readBytes->size()) + "\r";
				}
			}

			// Close the new file
			myNewfile.close();

			cout << "Array creation done. All " + std::to_string(readBytes->size()) + " bytes/characters are saved in " + newFileName << endl;
		}
		else
		{
			cout << "New file could not be created. Maybe running this tool with admin rights will solve this." << endl;
		}
	}

	system("pause");

	return 0;
}